function afficherNavBar() {
  var x = document.getElementById("contenuNavbar");

  var classes = x.classList;

  var classExiste = false;
  for (var i = 0; i < classes.length; i++) {
    if (classes[i] === "responsive") {
      classExiste = true;
    }
  }

  if (classExiste === false) {
    x.classList.add("responsive");
  } else {
    x.classList.remove("responsive");
  }
}

function fermerMenu() {
  var x = document.getElementById("contenuNavbar");
  x.classList.remove("responsive");
}